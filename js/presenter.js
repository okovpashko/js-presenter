require.config({
    baseUrl: 'js/lib',
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    },
    paths: {
        jquery: 'jquery-1.10.2.min',
        backbone: 'backbone-min',
        underscore: 'underscore-min'
    }
});

define('presenter', ['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var Presentation = Backbone.Model.extend({
        defaults: {
            slideId: 0,
            slideHtml: ""
        },
        loadSlide: function (slideId) {
            var self = this;
            require(['text!../../slides/' + slideId + '.html', '../../slides/' + slideId],
                function (slideTemplate, slideJS) {
                    if (slideTemplate.length > 0) {
                        var template = _.template(slideTemplate);
                        self.set({slideId: +slideId});
                        self.set({slideHtml: template(slideJS)});
                    }
                },
                function (err) {
                    var failedId = err.requireModules && err.requireModules[0];
                    if (failedId === '../../slides/' + slideId) {
                        require(['text!../../slides/' + slideId + '.html'],
                            function (slideTemplate) {
                                if (slideTemplate.length > 0) {
                                    self.set({slideId: +slideId});
                                    self.set({slideHtml: slideTemplate});
                                }
                            }
                        );
                    }
                }
            );
        },
        prevSlide: function () {
            this.loadSlide(this.get('slideId') - 1);
        },
        nextSlide: function () {
            this.loadSlide(this.get('slideId') + 1);
        }
    });
    var PresentationView = Backbone.View.extend({
        initialize: function () {
            this.model.on('change', this.render, this);
            this.model.on('change', function () {
                Backbone.history.navigate(this.model.get('slideId').toString());
            }, this);

            var self = this;
            $(document).on('keydown', function (e) {
                switch (e.keyCode) {
                    case 37:
                        self.model.prevSlide();
                        break;
                    case 39:
                        self.model.nextSlide();
                        break;
                }
            });
        },
        render: function () {
            this.$el.html('<div class="slides">' + this.model.get('slideHtml') + '</div>');
            this.$el.append('<div class="presenter-nav"><a id="presenter-prev" href="#">&lt</a><a id="presenter-next" href="#">&gt</a></div>');
        },
        events: {
            'click #presenter-prev': 'prevSlide',
            'click #presenter-next': 'nextSlide'
        },
        prevSlide: function (e) {
            e.preventDefault();
            this.model.prevSlide();
        },
        nextSlide: function (e) {
            e.preventDefault();
            this.model.nextSlide();
        }
    });
    return Backbone.Router.extend({
        routes: {
            '': "beginPresentation",
            ':slideId': "showSlide"
        },
        initialize: function (frame) {
            this.presentation = new Presentation();
            this.presentationView = new PresentationView({model: this.presentation, el: frame});
            Backbone.history.start();
        },
        beginPresentation: function () {
            this.showSlide(1);
        },
        showSlide: function (slideId) {
            this.presentation.loadSlide(slideId);
        },
        nextSlide: function () {
            this.presentation.nextSlide();
        },
        prevSlide: function () {
            this.presentation.prevSlide();
        }
    });
});